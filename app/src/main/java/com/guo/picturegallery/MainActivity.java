package com.guo.picturegallery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    /**
     * 本地设定的计划显示的网络图片资源<P/>
     */
    String[] pictures = {"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F201909%2F18%2F20190918210806_8nssN.thumb.700_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1620396123&t=e611b214970166ed1856fd9550a1f031",
            "https://pics3.baidu.com/feed/ca1349540923dd54d918df85ae64f4d89d824863.jpeg?token=c3be448f24d5cd73f5c8c445b042798a&s=65C89B47DBD2889C3B3835990300C012",
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.jj20.com%2Fup%2Fallimg%2Fmx08%2F0610191P500%2F1Z6101P500-1.jpg&refer=http%3A%2F%2Fpic.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1620396200&t=2428e0b0f6b19b48de8484af46bdad24",
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fd.fengfeng.cc%2Fnews%2Fyule%2F2017-07-18%2F71989998810a5493567619892c762fa5.jpg&refer=http%3A%2F%2Fd.fengfeng.cc&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1620396200&t=3946f9a42da5ec8432ecfa07bd0e151d"};
    String[] names = {"佟1","佟2","杨幂1","杨幂2"};

    String[] stars = {"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg3.yxlady.com%2Fyl%2FUploadFiles_5361%2F2015114%2F20151104230154701.jpg&refer=http%3A%2F%2Fimg3.yxlady.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624850393&t=5797f8b6328676d643a6fd71e2daa77b" ,
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.faxingsj.com%2Fuploads%2F2017-12%2F05-102818_903.jpg&refer=http%3A%2F%2Fwww.faxingsj.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624856003&t=cfcbdf4b562cd723fe3438422e96e39d",
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202002%2F17%2F20200217130820_awpcg.thumb.400_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624850393&t=1a7d3d5d51e8403afd9a5700b2aa3575" ,
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20170923%2F9cc6ccdee91148b2a21441188ca08833.jpeg&refer=http%3A%2F%2F5b0988e595225.cdn.sohucs.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624856003&t=8060f9631554359314aebfe4c01d9384",
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fcbu01.alicdn.com%2Fimg%2Fibank%2F2018%2F791%2F520%2F9272025197_1190069431.400x400.jpg&refer=http%3A%2F%2Fcbu01.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624850393&t=ef1cf747870fc800c1f4cb0f6f27971e" ,
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F201805%2F20%2F20180520143605_mhksn.thumb.400_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624850393&t=d51dbd424ef02292e298f105b5cce15f"};
    String[] starNames = {"佟丽娅","张俪","孙杨","张俪","杨幂","刘诗诗"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * 对RecyclerView初始化
         */
        RecyclerView recyclerViewGallery = findViewById(R.id.recycler_view_gallery);
        PictureAdapter adapter = new PictureAdapter();
        //设置Adapter需要放在addItemDecoration之前，否则无法子视图的宽度
        recyclerViewGallery.setAdapter(adapter);
        /**
         * 基于自定义的LayoutManager实现子视图在RecyclerView在屏幕的位置居中
         */
        LinearLayoutManager layoutManager = new GalleryLayoutManager(this,RecyclerView.HORIZONTAL,false);
        recyclerViewGallery.setLayoutManager(layoutManager);
//        recyclerViewGallery.addItemDecoration(new HorizontalDecoration(0));
        LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewGallery);
        recyclerViewGallery.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        /**
         * 点击屏幕中的非居中位置时，可以使点击的项目平滑移动至屏幕中央
         */
        adapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                recyclerViewGallery.smoothScrollToPosition(pos);
            }
        });
        adapter.setDataList(getStarsList());
    }

    private List<Picture> getAdapterList(){
        List<Picture> pictureList = new ArrayList<>();
        for(int i = 0 ; i < 50 ;i++){
            pictureList.add(new Picture( i , names[i%pictures.length] ,pictures[i%pictures.length]));
        }
        return pictureList;
    }

    private List<Picture> getStarsList(){
        List<Picture> pictureList = new ArrayList<>();
        for(int i = 0 ; i < 50 ;i++){
            pictureList.add(new Picture( i , starNames[i%starNames.length] , stars[i%stars.length]));
        }
        return pictureList;
    }
}