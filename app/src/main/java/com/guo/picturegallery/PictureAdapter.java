package com.guo.picturegallery;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

/**
 * 图片画廊的Adapter<P/>
 * @author mailanglideguozhe 20210520
 */
public class PictureAdapter extends BaseAdapter<Picture>{
    private static final String TAG = "PictureAdapter";
    @Override
    public void setData(ViewHolder holder,int pos) {
        if(null != getDataList().get(pos)){
            //实例化图片显示
            View view = holder.itemView;
            ImageView imageView = holder.imageView;
            Log.d(TAG,"position ="+pos+",current size = "+getDataList().size());
            //为专辑图片设置倒影显示
            new BitmapUtil().reflectionBitmap(getDataList().get(pos).url , imageView , new SimpleTarget<Bitmap>(){
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    imageView.setImageBitmap(resource);
                }
            });
            TextView textView = holder.textView;
            textView.setText(getDataList().get(pos).name);
        }
    }

    /**
     * 设置子视图的布局id
     * @return
     */
    @Override
    public int getId() {
        return R.layout.item_picture;
    }

    @Override
    public boolean areItemsTheSame(@NonNull Picture oldItem, @NonNull Picture newItem) {
        return oldItem.id == (newItem.id);
    }

    @Override
    public boolean areContentsTheSame(@NonNull Picture oldItem, @NonNull Picture newItem) {
        return oldItem.url.equals(newItem.url);
    }
}
